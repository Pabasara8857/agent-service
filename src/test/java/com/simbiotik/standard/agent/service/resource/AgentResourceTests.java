package com.simbiotik.standard.agent.service.resource;


import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;
import org.mockito.testng.MockitoTestNGListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simbiotik.standard.agent.service.dto.request.AddressRequestModel;
import com.simbiotik.standard.agent.service.dto.request.AgentRequestModel;
import com.simbiotik.standard.agent.service.dto.response.AddressResponseModel;
import com.simbiotik.standard.agent.service.dto.response.AgentResponseModel;
import com.simbiotik.standard.agent.service.service.AgentService;

/**
 * @author saurabh
 *
 */

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Listeners(MockitoTestNGListener.class)
public class AgentResourceTests extends AbstractTestNGSpringContextTests {

  MockMvc mockMvc;
	@Autowired
  public AgentResourceTests(MockMvc mockMvc) {
    this.mockMvc = mockMvc;
  }

	@MockBean
	AgentService agentService;

    @Autowired
    public AgentResourceTests(AgentService agentService) {
      this.agentService = agentService;
    }

    ObjectMapper objectMapper;
	@Autowired
    public AgentResourceTests(ObjectMapper objectMapper) {
      this.objectMapper = objectMapper;
    }

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void saveAgentTest() throws Exception {
		AgentRequestModel agentRequestModel = new AgentRequestModel(1L, "saurabh", "12345",
            new AddressRequestModel("Addr1", "Addr2", "pune", "mah", 12345), null);
		AgentResponseModel agentResponseModel = new AgentResponseModel(1L, "saurabh", "12345",
            new AddressResponseModel("Addr1", "Addr2", "pune", "mah", 12345), null);

		when(this.agentService.saveAgent(any(AgentRequestModel.class))).thenReturn(agentResponseModel);

		mockMvc.perform(post("/agents").content(asJsonString(agentRequestModel))
				.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.name", is(agentResponseModel.getName())))
				.andExpect(jsonPath("$.phoneNo", is(agentResponseModel.getPhoneNo())))
				.andExpect(jsonPath("$.address.addressLine1",
						is(agentResponseModel.getAddressResponseModel().getAddressLine1())))
				.andExpect(jsonPath("$.address.addressLine2",
						is(agentResponseModel.getAddressResponseModel().getAddressLine2())))
				.andExpect(jsonPath("$.address.city", is(agentResponseModel.getAddressResponseModel().getCity())))
				.andExpect(
						jsonPath("$.address.state",
								is(agentResponseModel.getAddressResponseModel().getState())))
				.andExpect(jsonPath("$.address.zipCode",
						is(agentResponseModel.getAddressResponseModel().getZipCode())));
	}

	@Test
	public void saveAgent400Test() throws Exception {
		AgentRequestModel agentRequestModel = new AgentRequestModel(1L, null, "12345",
            new AddressRequestModel("Addr1", "Addr2", "pune", "mah", 12345), null);
		mockMvc.perform(
				post("/agents").content(asJsonString(agentRequestModel)).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print()).andExpect(status().is4xxClientError());
	}

	@Test
	public void getAgentByIdTest() throws Exception {
		AgentResponseModel agentResponseModel = new AgentResponseModel(1L, "saurabh", "12345",
            new AddressResponseModel("Addr1", "Addr2", "pune", "mah", 12345), null);

		when(this.agentService.findAgentById(any(Long.class))).thenReturn(agentResponseModel);

		mockMvc.perform(get("/agents/1")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.name", is(agentResponseModel.getName())))
				.andExpect(jsonPath("$.phoneNo", is(agentResponseModel.getPhoneNo())))
				.andExpect(jsonPath("$.address.addressLine1",
						is(agentResponseModel.getAddressResponseModel().getAddressLine1())))
				.andExpect(jsonPath("$.address.addressLine2",
						is(agentResponseModel.getAddressResponseModel().getAddressLine2())))
				.andExpect(jsonPath("$.address.city", is(agentResponseModel.getAddressResponseModel().getCity())))
				.andExpect(
						jsonPath("$.address.state", is(agentResponseModel.getAddressResponseModel().getState())))
				.andExpect(
						jsonPath("$.address.zipCode", is(agentResponseModel.getAddressResponseModel().getZipCode())));
	}

	@Test
	public void getAgentById400Test() throws Exception {
		mockMvc.perform(get("/agents/null")).andExpect(status().is4xxClientError());
	}

	@Test
	public void updateAgentTest() throws Exception {
		AgentRequestModel agentRequestModel = new AgentRequestModel(1L, "xyz", "12345",
            new AddressRequestModel("Addr1", "Addr2", "pune", "mah", 12345), null);
		AgentResponseModel agentResponseModel = new AgentResponseModel(1L, "saurabh", "12345",
            new AddressResponseModel("Addr1", "Addr2", "pune", "mah", 12345), null);

		when(this.agentService.updateAgent(any(AgentRequestModel.class))).thenReturn(agentResponseModel);

		mockMvc.perform(put("/agents").content(asJsonString(agentRequestModel))
				.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.name", is(agentResponseModel.getName())))
				.andExpect(jsonPath("$.phoneNo", is(agentResponseModel.getPhoneNo())))
				.andExpect(jsonPath("$.address.addressLine1",
						is(agentResponseModel.getAddressResponseModel().getAddressLine1())))
				.andExpect(jsonPath("$.address.addressLine2",
						is(agentResponseModel.getAddressResponseModel().getAddressLine2())))
				.andExpect(jsonPath("$.address.city", is(agentResponseModel.getAddressResponseModel().getCity())))
				.andExpect(
						jsonPath("$.address.state", is(agentResponseModel.getAddressResponseModel().getState())))
				.andExpect(
						jsonPath("$.address.zipCode", is(agentResponseModel.getAddressResponseModel().getZipCode())));
	}

	@Test
	public void updateAgent400Test() throws Exception {
		AgentRequestModel agentRequestModel = new AgentRequestModel(1L, null, "12345",
            new AddressRequestModel("Addr1", "Addr2", "pune", "mah", 12345), null);
		mockMvc.perform(
				put("/agents").content(asJsonString(agentRequestModel)).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print()).andExpect(status().is4xxClientError());
	}

	@Test
	public void deleteAgentTest() throws Exception {
		when(this.agentService.deleteAgent(any(Long.class))).thenReturn(true);

		mockMvc.perform(delete("/agents/1")).andExpect(status().isOk()).andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$", is(true)));
	}

	@Test
	public void deleteAgent400Test() throws Exception {
		mockMvc.perform(delete("/agents/null")).andExpect(status().is4xxClientError()).andDo(print());
	}

	@Test
	public void findAllAgentsTest() throws Exception {
		AgentResponseModel agentResponseModel = new AgentResponseModel(1L, "saurabh", "12345",
            new AddressResponseModel("Addr1", "Addr2", "pune", "mah", 12345), null);
		Set<AgentResponseModel> agentRequestModels = Stream.of(agentResponseModel)
				.collect(Collectors.toUnmodifiableSet());

		when(this.agentService.findAllAgents()).thenReturn(agentRequestModels);

		mockMvc.perform(get("/agents")).andExpect(status().isOk()).andDo(print())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.[0].name", is(agentResponseModel.getName())))
				.andExpect(jsonPath("$.[0].phoneNo", is(agentResponseModel.getPhoneNo())))
				.andExpect(jsonPath("$.[0].address.addressLine1",
						is(agentResponseModel.getAddressResponseModel().getAddressLine1())))
				.andExpect(jsonPath("$.[0].address.addressLine2",
						is(agentResponseModel.getAddressResponseModel().getAddressLine2())))
				.andExpect(jsonPath("$.[0].address.city",
						is(agentResponseModel.getAddressResponseModel().getCity())))
				.andExpect(jsonPath("$.[0].address.state",
						is(agentResponseModel.getAddressResponseModel().getState())))
				.andExpect(jsonPath("$.[0].address.zipCode",
						is(agentResponseModel.getAddressResponseModel().getZipCode())));
	}

	@Test
	public void findAllAgents415Test() throws Exception {
		mockMvc.perform(post("/agents")).andExpect(status().is4xxClientError()).andDo(print());
	}

	@AfterMethod
	public void afterMethod() {
		reset(agentService);
	}

	public String asJsonString(final Object obj) {
		try {
			return this.objectMapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
