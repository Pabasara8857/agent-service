package com.simbiotik.standard.agent.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.mockito.testng.MockitoTestNGListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.simbiotik.standard.agent.service.resource.AgentResource;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Listeners(MockitoTestNGListener.class)
class AgentServiceApplicationTests extends AbstractTestNGSpringContextTests {

	@Autowired
	private AgentResource agentResource;

	@org.testng.annotations.Test(enabled = true)
	void contextLoads() {
		assertThat(agentResource).isNotNull().describedAs("agentResource is not null and is loaded");
	}

	@Test(enabled = true)
	public void check() {
		assertEquals("asdf", "asdf");
	}

}
