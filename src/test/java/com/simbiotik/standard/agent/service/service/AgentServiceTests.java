package com.simbiotik.standard.agent.service.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.simbiotik.standard.agent.service.exceptions.ItemNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.testng.MockitoTestNGListener;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.simbiotik.standard.agent.service.dto.converter.AddressConverter;
import com.simbiotik.standard.agent.service.dto.converter.AgentConverter;
import com.simbiotik.standard.agent.service.dto.request.AddressRequestModel;
import com.simbiotik.standard.agent.service.dto.request.AgentRequestModel;
import com.simbiotik.standard.agent.service.dto.response.AddressResponseModel;
import com.simbiotik.standard.agent.service.dto.response.AgentResponseModel;
import com.simbiotik.standard.agent.service.entity.Agent;
import com.simbiotik.standard.agent.service.entity.component.Address;
import com.simbiotik.standard.agent.service.repository.AgentRepository;

/**
 * @author saurabh
 *
 */

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Listeners(MockitoTestNGListener.class)
public class AgentServiceTests extends AbstractTestNGSpringContextTests {

	// Mock the service dependencies(=AgentService is dependent on
	// agentRepository)
	@Mock
	AgentRepository agentRepository;

	@Mock
	AgentConverter agentConverter;

	@Mock
	AddressConverter addressConverter;

	// Mock the service which is to be tested (Can't be an interface)
	@InjectMocks
	AgentService agentService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test(enabled = true)
	public void shouldReturnAgentResModel_ForGivenId() throws ItemNotFoundException {

		Long id = 1L;
		// Given
		Agent agent = new Agent(1L, "saurabh", "12345", new Address("Addr1", "Addr2", "pune", "mah", 12345));
		AddressResponseModel addressResponseModel = new AddressResponseModel("Addr1", "Addr2", "pune", "mah", 12345);
        AgentResponseModel agentResponseModel = new AgentResponseModel(null, "saurabh", "12345", addressResponseModel, null);

		when(this.agentRepository.findById(id)).thenReturn(Optional.of(agent));
		when(this.agentConverter.agentToAgentDto(agent)).thenReturn(agentResponseModel);

		// When
		AgentResponseModel testAgentResponseModel = this.agentService.findAgentById(1L);

		// Verify
		verify(this.agentRepository).findById(id);
		verify(this.agentConverter, times(1)).agentToAgentDto(any());
		verify(this.addressConverter, times(1)).addressToAddressDto(any());

		// Then
		Assert.assertEquals(testAgentResponseModel.getName(), "saurabh");
	}

	@Test(enabled = true, expectedExceptions = { ItemNotFoundException.class })
	public void getAgentById_ShouldThrowItemNotFoundExcp() throws ItemNotFoundException {

		// Given
		Long id = 1L;

		when(this.agentRepository.findById(id)).thenThrow(ItemNotFoundException.class);

		// When
		this.agentService.findAgentById(id);

		// Verify
		verify(this.agentRepository).findById(id);
		verify(this.agentConverter, times(0)).agentToAgentDto(any());
	}

	@Test(enabled = true, expectedExceptions = { IllegalArgumentException.class })
	public void getAgentById_ShouldThrowIllegalArgumentExcp() throws ItemNotFoundException {

		// Given
		Long id = 1L;

		when(this.agentRepository.findById(id)).thenThrow(IllegalArgumentException.class);

		// When
		this.agentService.findAgentById(id);

		// Verify
		verify(this.agentRepository).findById(id);
		verify(this.agentConverter, times(0)).agentToAgentDto(any());
	}

	@Test(enabled = true)
	public void shouldSaveAgent() {

		// Given
		AgentRequestModel agentRequestModel = new AgentRequestModel(1L, "saurabh", "12345",
            new AddressRequestModel("Addr1", "Addr2", "pune", "mah", 12345), null);
		Agent agent = new Agent(1L, "saurabh", "12345", new Address("Addr1", "Addr2", "pune", "mah", 12345));
		AddressResponseModel addressResponseModel = new AddressResponseModel("Addr1", "Addr2", "pune", "mah", 12345);
        AgentResponseModel agentResponseModel = new AgentResponseModel(1L, "saurabh", "12345", addressResponseModel, null);

		when(this.agentConverter.agentDtoToAgent(any(AgentRequestModel.class))).thenReturn(agent);
		when(this.agentRepository.save(any(Agent.class))).thenReturn(agent);
		when(this.agentConverter.agentToAgentDto(any(Agent.class))).thenReturn(agentResponseModel);

		// When
		AgentResponseModel testAgentResponseModel = this.agentService.saveAgent(agentRequestModel);

		// Verify
		verify(this.agentConverter).agentDtoToAgent(agentRequestModel);
		verify(this.agentRepository).save(agent);
		verify(this.agentConverter).agentToAgentDto(agent);

		// Then
		Assert.assertEquals(testAgentResponseModel.getName(), "saurabh");
	}

	@Test(enabled = true)
	public void shouldUpdateAgentForTheGivenId() throws ItemNotFoundException {

		// Given
		AgentRequestModel agentRequestModel = new AgentRequestModel(1L, "xyz", "12345",
            new AddressRequestModel("Addr1", "Addr2", "pune", "mah", 12345), null);
		Agent agent = new Agent(1L, "saurabh", "12345", new Address("Addr1", "Addr2", "pune", "mah", 12345));
		AddressResponseModel addressResponseModel = new AddressResponseModel("Addr1", "Addr2", "pune", "mah", 12345);
        AgentResponseModel agentResponseModel = new AgentResponseModel(1L, "xyz", "12345", addressResponseModel, null);


		when(this.agentRepository.findById(agentRequestModel.getId())).thenReturn(Optional.of(agent));
		when(this.agentRepository.save(any(Agent.class))).thenReturn(agent);
		when(this.agentConverter.agentToAgentDto(any(Agent.class))).thenReturn(agentResponseModel);
		when(this.addressConverter.addressToAddressDto(any(Address.class)))
				.thenReturn(agentResponseModel.getAddressResponseModel());

		Assert.assertNotEquals(agent.getName(), agentRequestModel.getName());

		// When
		AgentResponseModel testAgentResponseModel = this.agentService.updateAgent(agentRequestModel);

		// Verify
		verify(this.agentRepository).findById(agentRequestModel.getId());
		verify(this.agentRepository).save(agent);
		verify(this.agentConverter).agentToAgentDto(agent);
		verify(this.addressConverter).addressToAddressDto(agent.getAddress());

		// Then
		Assert.assertEquals(agent.getName(), agentRequestModel.getName());
		Assert.assertEquals(testAgentResponseModel.getName(), agent.getName());
	}

	@Test(enabled = true)
	public void shouldDeleteAgentForTheGivenId() throws ItemNotFoundException {

		// Given
		Long id = 1L;
		Agent agent = new Agent(1L, "saurabh", "12345", new Address("Addr1", "Addr2", "pune", "mah", 12345));

		when(this.agentRepository.findById(id)).thenReturn(Optional.of(agent));

		// When
		boolean testResult = this.agentService.deleteAgent(id);

		// Verify
		verify(this.agentRepository).findById(id);
		verify(this.agentRepository).deleteById(agent.getId());

		// Then
		Assert.assertEquals(testResult, true);
	}

	@Test(enabled = true)
	public void shouldFindAllAgents() throws ItemNotFoundException {

		// Given
		Agent agent1 = new Agent(1L, "00001", "Pen", new Address("Addr1", "Addr2", "pune", "mah", 12345));

		AgentResponseModel agentResponseModel1 = new AgentResponseModel(1L, "saurabh", "12345",
            new AddressResponseModel("Addr1", "Addr2", "pune", "mah", 12345), null);

		List<Agent> agentsList = Stream.of(agent1).collect(Collectors.toUnmodifiableList());
		Set<AgentResponseModel> agentsAgentResponseModelsSet = Stream.of(agentResponseModel1)
				.collect(Collectors.toUnmodifiableSet());

		when(this.agentRepository.findAll()).thenReturn(agentsList);
		when(this.agentConverter.agentToAgentDto(agent1)).thenReturn(agentResponseModel1);

		// When
		Set<AgentResponseModel> testAgentResponseModelsSet = this.agentService.findAllAgents();

		// Verify
		verify(this.agentRepository).findAll();
		verify(this.agentConverter, times(1)).agentToAgentDto(any(Agent.class));

		// Then
		Assert.assertEquals(testAgentResponseModelsSet, agentsAgentResponseModelsSet);
	}

//	BDDMockito.given(this.agentRepository.save(agent)).will(invocation -> invocation.getArgument(0));
//	doReturn(new Agent()).when(this.agentRepository.save(any(Agent.class)));

}
