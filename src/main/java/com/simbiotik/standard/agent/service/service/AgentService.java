package com.simbiotik.standard.agent.service.service;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import com.simbiotik.standard.agent.service.exceptions.ItemNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.simbiotik.standard.agent.service.dto.converter.AddressConverter;
import com.simbiotik.standard.agent.service.dto.converter.AgentConverter;
import com.simbiotik.standard.agent.service.dto.converter.AgentProductsConverter;
import com.simbiotik.standard.agent.service.dto.request.AgentProductsRequestModel;
import com.simbiotik.standard.agent.service.dto.request.AgentRequestModel;
import com.simbiotik.standard.agent.service.dto.response.AddressResponseModel;
import com.simbiotik.standard.agent.service.dto.response.AgentProductsResponseModel;
import com.simbiotik.standard.agent.service.dto.response.AgentResponseModel;
import com.simbiotik.standard.agent.service.entity.Agent;
import com.simbiotik.standard.agent.service.entity.AgentProducts;
import com.simbiotik.standard.agent.service.entity.component.Address;
import com.simbiotik.standard.agent.service.repository.AgentProductsRepository;
import com.simbiotik.standard.agent.service.repository.AgentRepository;
import com.simbiotik.standard.common.service.ApplicationConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AgentService {

    private AgentRepository agentRepository;
    private AgentConverter agentConverter;
    private AddressConverter addressConverter;
    private AgentProductsConverter agentProductsConverter;
    private AgentProductsRepository agentProductsRepository;
    private MessageSource messageSource;

    @Autowired
    public AgentService(AgentRepository agentRepository, AgentConverter agentConverter, AddressConverter addressConverter, AgentProductsConverter agentProductsConverter,
        AgentProductsRepository agentProductsRepository, MessageSource messageSource) {
      this.agentRepository = agentRepository;
      this.agentConverter = agentConverter;
      this.addressConverter = addressConverter;
      this.agentProductsConverter = agentProductsConverter;
      this.agentProductsRepository = agentProductsRepository;
      this.messageSource = messageSource;
    }

    /**
     * This method is used to save agent object
     * @author Pabasara Wijerathne
     * @param agentRequestModel
     * @return AgentResponseModel object
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public AgentResponseModel saveAgent(AgentRequestModel agentRequestModel) {
        Set<AgentProductsRequestModel> agentProductsRequestModels = agentRequestModel.getAgentProductsRequestModels();
        Agent agent = agentConverter.agentDtoToAgent(agentRequestModel);

        Address address = addressConverter.addressDtoToAddress(agentRequestModel.getAddressRequestModel());
        agent.setAddress(address);

        if(!CollectionUtils.isEmpty(agent.getAgentProducts())) agent.getAgentProducts().clear();
        agent = agentRepository.save(agent);

        AgentResponseModel agentResponseModel = agentConverter.agentToAgentDto(agent);
        if(!CollectionUtils.isEmpty(agentProductsRequestModels)) {
            Set<AgentProductsResponseModel> agentProductsResponseModels = saveAgentProducts(agent, agentProductsRequestModels);
            agentResponseModel.setAgentProductsResponseModels(agentProductsResponseModels);
        }
        AddressResponseModel addressResponseModel = addressConverter.addressToAddressDto(agent.getAddress());
        agentResponseModel.setAddressResponseModel(addressResponseModel);
        return agentResponseModel;
    }

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Set<AgentProductsResponseModel> saveAgentProducts(Agent agent, Set<AgentProductsRequestModel> agentProductsRequestModels){
        Set<AgentProducts> agentProductsSet = new HashSet<>();
        agentProductsRequestModels.forEach(agentProductsRequest ->{
            AgentProducts agentProducts = agentProductsConverter.agentProductsDtoToAgentProducts(agentProductsRequest);
            agentProducts.setAgent(agent);
            agentProductsSet.add(agentProducts);
        });

        List<AgentProducts> agentProductsList = agentProductsRepository.saveAll(agentProductsSet);

        Set<AgentProductsResponseModel> agentProductsResponseModels = new HashSet<>();
        agentProductsList.forEach(agentProduct -> {
            agentProductsResponseModels.add(agentProductsConverter.agentProductsToAgentProductsDto(agentProduct));
        });
        return agentProductsResponseModels;
    }

    /**
     * This method is used to fetch agent object by id
     * @author Pabasara Wijerathne
     * @param id
     * @return AgentResponseModel object
     * @exception ItemNotFoundException on agent not found error
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public AgentResponseModel findAgentById(long id) {
        Agent agent = agentRepository.findById(id).orElseThrow(() -> new ItemNotFoundException(messageSource.getMessage(ApplicationConstants.AGENT_NOT_FOUND_FOR_ID, new Object[] { id }, Locale
            .getDefault())));
        AgentResponseModel agentResponseModel = agentConverter.agentToAgentDto(agent);
        AddressResponseModel addressResponseModel = addressConverter.addressToAddressDto(agent.getAddress());
        agentResponseModel.setAddressResponseModel(addressResponseModel);
        return agentResponseModel;
    }

    /**
     * This method is used to fetch all agent objects
     * @author Pabasara Wijerathne
     * @param
     * @return Set of AgentResponseModel objects
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Set<AgentResponseModel> findAllAgents() {
        Set<AgentResponseModel> agentResponseModels = new HashSet<>();
        List<Agent> agentList = agentRepository.findAll();
        agentList.forEach(agent -> {
            AgentResponseModel agentResponseModel = agentConverter.agentToAgentDto(agent);
            AddressResponseModel addressResponseModel = addressConverter.addressToAddressDto(agent.getAddress());
            agentResponseModel.setAddressResponseModel(addressResponseModel);
            agentResponseModels.add(agentResponseModel);
        });
        return agentResponseModels;
    }

    /**
     * This method is used to update agent object
     * @author Pabasara Wijerathne
     * @param agentRequestModel
     * @return AgentResponseModel object
     * @exception ItemNotFoundException on agent not found error
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public AgentResponseModel updateAgent(AgentRequestModel agentRequestModel) {
        Agent agent = agentRepository.findById(agentRequestModel.getId()).orElseThrow(() ->
            new ItemNotFoundException(messageSource.getMessage(ApplicationConstants.AGENT_NOT_FOUND_FOR_ID, new Object[] {
                agentRequestModel.getId() }, Locale.getDefault())));
        updateAgentDetails(agent, agentRequestModel);
        agent = agentRepository.save(agent);
        AgentResponseModel agentResponseModel = agentConverter.agentToAgentDto(agent);
        AddressResponseModel addressResponseModel = addressConverter.addressToAddressDto(agent.getAddress());
        agentResponseModel.setAddressResponseModel(addressResponseModel);
        return agentResponseModel;
    }

    /**
     * This method is used to delete agent object
     * @author Pabasara Wijerathne
     * @param id
     * @return boolean This returns true if deletion is success
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public boolean deleteAgent(long id) {
        Agent agent = agentRepository.findById(id).orElseThrow(() -> new ItemNotFoundException(messageSource.getMessage(ApplicationConstants.AGENT_NOT_FOUND_FOR_ID, new Object[] {
            id }, Locale.getDefault())));
        agentRepository.deleteById(agent.getId());
        return true;
    }

    /**
     * This method is used to update agent details
     * @author Pabasara Wijerathne
     * @param agent, agentRequestModel
     * @return Agent object
     */
    public Agent updateAgentDetails(Agent agent, AgentRequestModel agentRequestModel){
        agent.setName(agentRequestModel.getName());
        agent.setPhoneNo(agentRequestModel.getPhoneNo());

        Address address = new Address();
        address.setAddressLine1(agentRequestModel.getAddressRequestModel().getAddressLine1());
        address.setAddressLine2(agentRequestModel.getAddressRequestModel().getAddressLine2());
        address.setCity(agentRequestModel.getAddressRequestModel().getCity());
        address.setState(agentRequestModel.getAddressRequestModel().getState());
        address.setZipCode(agentRequestModel.getAddressRequestModel().getZipCode());
        agent.setAddress(address);

        return agent;
    }
}
