package com.simbiotik.standard.agent.service.dto.request;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The AgentProductsRequestModel class will be used to represent agent products
 * request model
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Component
@Getter
@Setter
@NoArgsConstructor
@Schema(title = "Agent Products Request Model", description = "Represent agent products request model")
public class AgentProductsRequestModel {

    @Schema(title = "AgentProducts id",  description = "Auto generated Id",  required = true)
    @JsonProperty("id")
    private Long id;

    @NotNull(message = "Product id is required")
    @Schema(title = "Product Id",  description = "Id of the product")
    @JsonProperty("productId")
    private Long productId;

    @Schema(title = "Agent Response Model",  description = "Response model of the agent")
    @JsonProperty("agent")
    private AgentRequestModel agentRequestModel;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AgentProductsRequestModel that = (AgentProductsRequestModel) o;
        return Objects.equals(id, that.id) && Objects.equals(productId, that.productId) && Objects.equals(agentRequestModel, that.agentRequestModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productId, agentRequestModel);
    }
}
