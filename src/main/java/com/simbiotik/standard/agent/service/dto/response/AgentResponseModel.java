package com.simbiotik.standard.agent.service.dto.response;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The AgentResponseModel class will be used to represent agent response model
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "Agent Response Model", description = "Represent agent response model")
public class AgentResponseModel extends RepresentationModel<AgentResponseModel> implements Serializable {

    @Schema(title = "Agent id",  description = "Auto generated Id")
    @JsonProperty("id")
    private Long id;

    @Schema(title = "Agent name",  description = "Min 2 characters & Max allowed 50 characters")
    @JsonProperty("name")
    private String name;

    @Schema(title = "Agent phone no",  description = "Max allowed 15 characters")
    @JsonProperty("phoneNo")
    private String phoneNo;

    @Schema(title = "Agent address",  description = "Address component")
    @JsonProperty("address")
    private AddressResponseModel addressResponseModel;

    @JsonProperty("agentProducts")
    private Set<AgentProductsResponseModel> agentProductsResponseModels;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        AgentResponseModel that = (AgentResponseModel) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(phoneNo, that.phoneNo) && Objects
            .equals(addressResponseModel, that.addressResponseModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name, phoneNo, addressResponseModel);
    }
}
