package com.simbiotik.standard.agent.service.entity;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.simbiotik.standard.agent.service.entity.component.Address;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Agent class is used to define attributes Agent entity
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "AGENT",
    indexes = {
        @Index(name = "INDEX_NAME",  columnList="NAME", unique = false),
        @Index(name = "INDEX_PHONE_NO",  columnList="PHONE_NO", unique = true)},
    uniqueConstraints = {
        @UniqueConstraint(name = "UNI_PHONE_NO", columnNames = "PHONE_NO")})
public class Agent extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * auto generated id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    /**
     * name
     */
    @Column(name = "NAME", length = 50, nullable = false)
    private String name;

    /**
     * phone no
     */
    @Column(name = "PHONE_NO", length = 15, nullable = false)
    private String phoneNo;

    /**
     * agentProducts
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "agent")
    private Set<AgentProducts> agentProducts;

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name="addressLine1",
            column=@Column(name = "ADDRESS_LINE_1", length = 300, nullable = false)),
        @AttributeOverride(name="addressLine2",
            column=@Column(name = "ADDRESS_LINE_2", length = 300, nullable = true)),
        @AttributeOverride(name="city",
            column=@Column(name = "CITY", length = 100, nullable = false)),
        @AttributeOverride(name="state",
            column=@Column(name = "STATE", length = 100, nullable = false)),
        @AttributeOverride(name="zipCode",
            column=@Column(name = "ZIP_CODE", length = 10, nullable = true))
    })
    private Address address;

    public Agent(Long id, String name, String phoneNo, Address address) {
        this.id = id;
        this.name = name;
        this.phoneNo = phoneNo;
        this.address = address;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        Agent agent = (Agent) o;
        return Objects.equals(id, agent.id) && Objects.equals(name, agent.name) && Objects.equals(phoneNo, agent.phoneNo) && Objects.equals(address, agent.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name, phoneNo, address);
    }
}
