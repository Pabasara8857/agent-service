package com.simbiotik.standard.agent.service.dto.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simbiotik.standard.agent.service.dto.request.AddressRequestModel;
import com.simbiotik.standard.agent.service.dto.response.AddressResponseModel;
import com.simbiotik.standard.agent.service.entity.component.Address;

import lombok.NoArgsConstructor;

/**
 * The AddressConverter class will be used to convert entity to dto and dto to
 * entity
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class AddressConverter {

    private ModelMapper modelMapper;

    @Autowired
    public AddressConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    /**
     * This method is used to convert address dto to address entity
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @return address entity
     */
    public Address addressDtoToAddress(AddressRequestModel addressRequestModel){
        return this.modelMapper.map(addressRequestModel, Address.class);
    }

    /**
     * This method is used to convert address dto to address entity
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @return address entity
     */
    public AddressResponseModel addressToAddressDto(Address address){
        return this.modelMapper.map(address, AddressResponseModel.class);
    }

}
