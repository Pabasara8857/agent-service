package com.simbiotik.standard.agent.service.dto.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simbiotik.standard.agent.service.dto.request.AgentRequestModel;
import com.simbiotik.standard.agent.service.dto.response.AgentResponseModel;
import com.simbiotik.standard.agent.service.entity.Agent;

import lombok.NoArgsConstructor;

/**
 * The AgentConverter class will be used to convert entity to dto and dto to
 * entity
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class AgentConverter {

    private ModelMapper modelMapper;

    @Autowired
    public AgentConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    /**
     * This method is used to convert agent dto to agent entity
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @return agent entity
     */
    public Agent agentDtoToAgent(AgentRequestModel agentRequestModel){
        return this.modelMapper.map(agentRequestModel, Agent.class);
    }

    /**
     * This method is used to convert agent dto to agent entity
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @return agent entity
     */
    public AgentResponseModel agentToAgentDto(Agent agent){
        return this.modelMapper.map(agent, AgentResponseModel.class);
    }

}
