package com.simbiotik.standard.agent.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.simbiotik.standard.agent.service.entity.AgentProducts;

/**
 * The AgentProductsRepository interface will be used for storing AgentProducts
 * details
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Repository
public interface AgentProductsRepository extends JpaRepository<AgentProducts, Long> {

    @Query("from AgentProducts ap where ap.agent.id = :id")
    public List<AgentProducts> findByAgentId(@Param("id") long id);
}
