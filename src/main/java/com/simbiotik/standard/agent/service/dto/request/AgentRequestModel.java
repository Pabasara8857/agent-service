package com.simbiotik.standard.agent.service.dto.request;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The AgentRequestModel class will be used to represent agent request model
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "Agent Request Model", description = "Represent agent request model")
public class AgentRequestModel implements Serializable {

    @Schema(title = "This field is to enter agent id",  description = "Auto generated Id", required = true)
    @JsonProperty("id")
    private Long id;

    @NotNull(message = "Name is required")
    @Schema(title = "This field is to enter agent name",  description = "Min 2 characters & Max allowed 50 characters", required = true)
    @Size(min = 2, max=50)
    @JsonProperty("name")
    private String name;

    @NotNull(message = "Phone No is required")
    @Schema(title = "This field is to enter agent phone no",  description = "Max allowed 15 characters", required = true)
    @Size(max=15)
    @JsonProperty("phoneNo")
    private String phoneNo;

    @NotNull(message = "Address is required")
    @Schema(title = "This field is to enter agent address",  description = "Address component", required = true)
    @JsonProperty("address")
    private AddressRequestModel addressRequestModel;

    @JsonProperty("agentProducts")
    private Set<AgentProductsRequestModel> agentProductsRequestModels;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AgentRequestModel that = (AgentRequestModel) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(phoneNo, that.phoneNo) && Objects
            .equals(addressRequestModel, that.addressRequestModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, phoneNo, addressRequestModel);
    }
}
