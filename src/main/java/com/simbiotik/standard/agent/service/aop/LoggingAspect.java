package com.simbiotik.standard.agent.service.aop;

import com.simbiotik.standard.agent.service.dto.request.AgentRequestModel;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    @Pointcut("within(com.simbiotik.standard.agent.service.service..*)")
    public void servicePackagePointcut(){}

    @Pointcut("execution(* com.simbiotik.standard.agent.service.service.AgentService.saveAgent(..))")
    public void saveAgentPointcut(){}


    @Before("servicePackagePointcut()")
    public void logBefore(JoinPoint joinPoint){
        log.debug("Method {}.{}() is going to start..", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName());
    }

    @After("servicePackagePointcut()")
    public void logAfter(JoinPoint joinPoint){
        log.debug("Method {}.{}() end.", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName());
    }

    @Before("saveAgentPointcut()")
    public void logBeforeSaveAgent(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        AgentRequestModel agentRequestModel = (AgentRequestModel) args[0];
        log.debug("Going to save agent : " + agentRequestModel);
    }

    @AfterThrowing(pointcut = "servicePackagePointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e){
        log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName(), e.getMessage() != null ? e.getMessage() : "NULL");
    }
}
