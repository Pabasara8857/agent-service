package com.simbiotik.standard.agent.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.simbiotik.standard.agent.service.entity.Agent;

/**
 * The AgentRepository interface will be used for storing Agent details
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Repository
public interface AgentRepository extends JpaRepository<Agent, Long> {
}
