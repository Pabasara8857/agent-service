package com.simbiotik.standard.agent.service.resource;

import com.simbiotik.standard.agent.service.service.AgentProductsService;
import com.simbiotik.standard.agent.service.service.ProductRemoteCallService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/products")
public class ProductResource {

    @Autowired
    private AgentProductsService agentProductsService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Map<String, Object>> getProductsByAgentId(@PathVariable("id") final long id){
        log.info("Call getProductsByAgentId : " + id);
        return agentProductsService.getProductsByAgentId(id);
    }
}
