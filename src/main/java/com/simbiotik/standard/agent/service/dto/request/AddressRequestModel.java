package com.simbiotik.standard.agent.service.dto.request;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The AddressRequestModel class will be used to represent address request model
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "Address Request Model", description = "Represent address request model")
public class AddressRequestModel implements Serializable {
    @NotNull(message = "Address Line 1 is required")
    @Schema(title = "This field is to enter address line 1",  description = "Min 2 characters & Max allowed 300 characters", required = true)
    @Size(min = 2, max=300)
    @JsonProperty("addressLine1")
    private String addressLine1;

    @Schema(title = "This field is to enter address line 2",  description = "Min 2 characters & Max allowed 300 characters", required = false)
    @Size(min = 2, max=300)
    @JsonProperty("addressLine2")
    private String addressLine2;

    @NotNull(message = "City is required")
    @Schema(title = "This field is to enter city",  description = "Min 2 characters & Max allowed 100 characters", required = true)
    @Size(min = 2, max=100)
    @JsonProperty("city")
    private String city;

    @NotNull(message = "State is required")
    @Schema(title = "This field is to enter state",  description = "Min 2 characters & Max allowed 100 characters", required = true)
    @Size(min = 2, max=100)
    @JsonProperty("state")
    private String state;

    @Schema(title = "This field is to enter zipcode",  description = "Max allowed 10 characters", required = false)
    @Size(max=10)
    @JsonProperty("zipCode")
    private int zipCode;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AddressRequestModel that = (AddressRequestModel) o;
        return zipCode == that.zipCode && Objects.equals(addressLine1, that.addressLine1) && Objects.equals(addressLine2, that.addressLine2) && Objects.equals(city, that.city)
            && Objects.equals(state, that.state);
    }

    @Override public int hashCode() {
        return Objects.hash(addressLine1, addressLine2, city, state, zipCode);
    }
}
