package com.simbiotik.standard.agent.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;

/**
 * The ApiDocumentationConfig class will be used for configuration
 * 
 * @author paabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Configuration
public class ApiDocumentationConfig {
    @Bean
    public OpenAPI customOpenAPI(/*@Value("${application-description}") String appDesciption,
        @Value("${application-version}") String appVersion*/) {
        return new OpenAPI()
            .info(new Info()
                .title("Agent Service API")
                .version("1.0.0")
                .contact(new Contact().name("Pabasara Wijerathne").email("pabasara@simbiotiktech.com"))
                .description("APIs for Agent")
                .termsOfService("http://hla.io/terms/")
                .license(new License().name("Apache 2.0").url("http://www.apache.org/licenses/LICENSE-2.0.html")))
            .addServersItem(new Server()
                .url("http://localhost:8123/agent-service").description("Local Server"));
    }
}