package com.simbiotik.standard.agent.service.resource;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Locale;
import java.util.Set;

import javax.validation.Valid;

import com.simbiotik.standard.agent.service.dto.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simbiotik.standard.agent.service.dto.request.AgentRequestModel;
import com.simbiotik.standard.agent.service.dto.response.AgentResponseModel;
import com.simbiotik.standard.agent.service.service.AgentService;
import com.simbiotik.standard.common.service.ApplicationConstants;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.links.Link;
import io.swagger.v3.oas.annotations.links.LinkParameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;

/**
 * The AgentResource class will be used for control Agent operations
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Slf4j
@RestController
@RequestMapping("/agents")
public class AgentResource {


    private AgentService agentService;
    private MessageSource messageSource;

    /**
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param agentService -> initializing agentService
     * @param messageSource -> initializing messageSource
     */
    @Autowired
    public AgentResource(AgentService agentService, MessageSource messageSource) {
        this.agentService = agentService;
        this.messageSource = messageSource;
    }

    /**
     * This method is used to create new agent
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param agentRequestModel -> the details of new product which we want to
     *          add
     * @return baseResponse
     */
    @Operation(summary = "Create Agent", security = @SecurityRequirement(name = "ALLOWED_ROLE",
        scopes = "ROLE_USER"))
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "Created the agent",
            content = { @Content(mediaType = "application/json",
                schema = @Schema(implementation = AgentResponseModel.class)) },
            links = { @Link(name = "Find agent by id", operationId = "findAgentById",
                parameters = { @LinkParameter(name = "id", expression = "$request.query.id")})}),
        @ApiResponse(responseCode = "400", description = "Invalid agent id", content = @Content),
        @ApiResponse(responseCode = "404", description = "Agent not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)})
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> createAgent(@Valid @RequestBody AgentRequestModel agentRequestModel){
        AgentResponseModel agentResponseModel = agentService.saveAgent(agentRequestModel);
        agentResponseModel.add(
            linkTo(methodOn(AgentResource.class).findAgentById(agentResponseModel.getId())).withRel(ApplicationConstants.GET),
            linkTo(methodOn(AgentResource.class).deleteAgent(agentResponseModel.getId())).withRel(ApplicationConstants.DELETE),
            linkTo(methodOn(AgentResource.class).findAllAgents()).withRel("GET: get all agents"));
        BaseResponse baseResponse = new BaseResponse(HttpStatus.CREATED.value(), messageSource.getMessage("agent.created.successfully", null, Locale.getDefault()), agentResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
    }

    /**
     * This method is used to fetch all agents
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @return baseResponse
     */
    @Operation(summary = "Get all Agents", security = @SecurityRequirement(name = "ALLOWED_ROLE",
        scopes = "ROLE_USER"))
    @ApiResponses(value = {
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content),
        @ApiResponse(responseCode = "400", description = "Invalid agent id", content = @Content),
        @ApiResponse(responseCode = "404", description = "Agent not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> findAllAgents(){
        Set<AgentResponseModel> agentResponseModels = agentService.findAllAgents();
        agentResponseModels.forEach(agentResponseModel ->{
            agentResponseModel.add(
              linkTo(methodOn(AgentResource.class).findAgentById(agentResponseModel.getId())).withRel(ApplicationConstants.GET),
              linkTo(methodOn(AgentResource.class).deleteAgent(agentResponseModel.getId())).withRel(ApplicationConstants.DELETE));
        });
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("agent.retrieved.all.successfully", null, Locale.getDefault()) , agentResponseModels);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to fetch agent for provided id
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param id -> agent id
     * @return baseResponse
     */
    @Operation(summary = "Get Agent by Id", security = @SecurityRequirement(name = "ALLOWED_ROLE",
        scopes = "ROLE_USER"))
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the agent",
            content = { @Content(mediaType = "application/json",
                schema = @Schema(implementation = AgentResponseModel.class)) }),
        @ApiResponse(responseCode = "400", description = "Invalid agent id", content = @Content),
        @ApiResponse(responseCode = "404", description = "Agent not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)})
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> findAgentById(@PathVariable("id") final long id){
        AgentResponseModel agentResponseModel = agentService.findAgentById(id);
        agentResponseModel.add(
            linkTo(methodOn(AgentResource.class).findAgentById(agentResponseModel.getId())).withRel(ApplicationConstants.GET),
            linkTo(methodOn(AgentResource.class).deleteAgent(agentResponseModel.getId())).withRel(ApplicationConstants.DELETE));
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("agent.found.successfully", null, Locale.getDefault()), agentResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to update the existing agent
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param agentRequestModel -> the details of product which we want to
     *          update
     * @return baseResponse with updated agent entity
     */
    @Operation(summary = "Update Agent", security = @SecurityRequirement(name = "ALLOWED_ROLE",
        scopes = "ROLE_USER"))
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Updated the agent",
            content = { @Content(mediaType = "application/json",
                schema = @Schema(implementation = AgentResponseModel.class)) }),
        @ApiResponse(responseCode = "404", description = "Agent not found", content = @Content),
        @ApiResponse(responseCode = "400", description = "Invalid agent id", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)})
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> updateAgent(@Valid @RequestBody AgentRequestModel agentRequestModel){
        AgentResponseModel agentResponseModel = agentService.updateAgent(agentRequestModel);
        agentResponseModel.add(
            linkTo(methodOn(AgentResource.class).findAgentById(agentResponseModel.getId())).withRel(ApplicationConstants.GET),
            linkTo(methodOn(AgentResource.class).deleteAgent(agentResponseModel.getId())).withRel(ApplicationConstants.DELETE),
            linkTo(methodOn(AgentResource.class).findAllAgents()).withRel("GET: get all agents"));
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("agent.updated.successfully", null, Locale.getDefault()), agentResponseModel);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /**
     * This method is used to delete the existing agent
     *
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param id -> agent id
     * @return baseResponse
     */
    @Operation(summary = "Delete Agent", security = @SecurityRequirement(name = "ALLOWED_ROLE",
        scopes = "ROLE_USER"))
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Deleted the agent", content = @Content),
        @ApiResponse(responseCode = "400", description = "Invalid agent id", content = @Content),
        @ApiResponse(responseCode = "404", description = "Agent not found", content = @Content),
        @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)})
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> deleteAgent(@PathVariable("id") final long id){
        Boolean isDeleted = agentService.deleteAgent(id);
        BaseResponse baseResponse = new BaseResponse(HttpStatus.OK.value(), messageSource.getMessage("agent.deleted.successfully", null, Locale.getDefault()), isDeleted);
        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    /*@Value("${client.pseudo.property}")
    private String pseudoProperty;

    @GetMapping("/property")
    public ResponseEntity<String> getProperty() {
        return ResponseEntity.ok(pseudoProperty);
    }*/

    @Value("${environment.property}")
    private String environment;

    @GetMapping("/property-env")
    public ResponseEntity<String> getEnvironmentProperty() {
        return ResponseEntity.ok(environment);
    }

}
