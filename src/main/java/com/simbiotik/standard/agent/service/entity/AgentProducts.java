package com.simbiotik.standard.agent.service.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The AgentProducts class is used to define attributes AgentProducts entity
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "AGENT_PRODUCTS")
public class AgentProducts extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * auto generated id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    /**
     * product id
     */
    @Column(name = "PRODUCT_ID", nullable = false)
    private Long productId;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "AGENT_ID",
        foreignKey = @ForeignKey(name = "fk_agent_with_product"), referencedColumnName = "ID")
    private Agent agent;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        AgentProducts that = (AgentProducts) o;
        return Objects.equals(id, that.id) && Objects.equals(productId, that.productId) && Objects.equals(agent, that.agent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, productId, agent);
    }
}
