package com.simbiotik.standard.agent.service.dto.response;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The AddressResponseModel class will be used to represent address response
 * model
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Component
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Schema(title = "Address Response Model", description = "Represent address response model")
public class AddressResponseModel implements Serializable {

    @Schema(title = "Address line 1",  description = "Min 2 characters & Max allowed 300 characters")
    @JsonProperty("addressLine1")
    private String addressLine1;

    @Schema(title = "Address line 2",  description = "Min 2 characters & Max allowed 300 characters")
    @JsonProperty("addressLine2")
    private String addressLine2;

    @Schema(title = "City",  description = "Min 2 characters & Max allowed 100 characters")
    @JsonProperty("city")
    private String city;

    @Schema(title = "State",  description = "Min 2 characters & Max allowed 100 characters")
    @JsonProperty("state")
    private String state;

    @Schema(title = "ZipCode",  description = "Max allowed 10 characters")
    @JsonProperty("zipCode")
    private int zipCode;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        AddressResponseModel that = (AddressResponseModel) o;
        return zipCode == that.zipCode && Objects.equals(addressLine1, that.addressLine1) && Objects.equals(addressLine2, that.addressLine2) && Objects.equals(city, that.city)
            && Objects.equals(state, that.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressLine1, addressLine2, city, state, zipCode);
    }
}
