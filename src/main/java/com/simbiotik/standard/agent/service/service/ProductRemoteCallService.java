package com.simbiotik.standard.agent.service.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@FeignClient(name="product-service")
//@FeignClient(name="product-service", url="http://localhost:8124/product-service/products")
public interface ProductRemoteCallService {

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Map<String, Object>> getProductsByAgentId(@PathVariable("id") long id);
}
