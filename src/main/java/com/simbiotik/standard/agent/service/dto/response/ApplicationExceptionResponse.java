/**
 * 
 */
package com.simbiotik.standard.agent.service.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * The Application Exception Response class will be used to respond exceptions
 * occurring in operations
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationExceptionResponse {

	private Object errorMessage;
	private LocalDateTime timestamp;
	private String description;
	private int returnCode;
}
