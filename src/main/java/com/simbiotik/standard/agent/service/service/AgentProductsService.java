package com.simbiotik.standard.agent.service.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.simbiotik.standard.agent.service.entity.AgentProducts;
import com.simbiotik.standard.agent.service.repository.AgentProductsRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * The AgentProductsService class will be used for handle agent products
 * operations
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Slf4j
@Service
public class AgentProductsService {

  /**
   * @author pabasara@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param agentProductsRepository -> initializing agentProductsRepository
   * @param productRemoteCallService -> initializing productRemoteCallService
   * 
   */
    private AgentProductsRepository agentProductsRepository;
  private ProductRemoteCallService productRemoteCallService;
    @Autowired
  public AgentProductsService(AgentProductsRepository agentProductsRepository, ProductRemoteCallService productRemoteCallService, MessageSource messageSource) {
    this.agentProductsRepository = agentProductsRepository;
    this.productRemoteCallService = productRemoteCallService;
  }

  /**
   * This method is used to get the products
   * 
   * @author pabasara@simbiotiktech.com
   * @version 1.0
   * @since 1.0
   * @param id -> agent id
   * @return products
   */
    public ResponseEntity<Map<String, Object>> getProductsByAgentId(long id){
        List<AgentProducts> agentProducts = findAgentProductsByAgentId(id);
        return productRemoteCallService.getProductsByAgentId(18);
        //return productRemoteCallService.getProductsByAgentId(agentProducts.get(0).getProductId());
    }

    /**
     * This method is used to get the agent products
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @param id -> agent id
     * @return agent products
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<AgentProducts> findAgentProductsByAgentId(long id) {
        return agentProductsRepository.findByAgentId(id);
    }

}
