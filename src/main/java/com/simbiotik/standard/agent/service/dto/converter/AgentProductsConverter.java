package com.simbiotik.standard.agent.service.dto.converter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simbiotik.standard.agent.service.dto.request.AgentProductsRequestModel;
import com.simbiotik.standard.agent.service.dto.response.AgentProductsResponseModel;
import com.simbiotik.standard.agent.service.entity.AgentProducts;

import lombok.NoArgsConstructor;

/**
 * The AgentProductsConverter class will be used to convert entity to dto and
 * dto to entity
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Component
@NoArgsConstructor
public class AgentProductsConverter {

    private ModelMapper modelMapper;

    @Autowired
    public AgentProductsConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    /**
     * This method is used to convert agentProducts dto to agentProducts entity
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @return agentProducts entity
     */
    public AgentProducts agentProductsDtoToAgentProducts(AgentProductsRequestModel agentProductsRequestModel){
        return this.modelMapper.map(agentProductsRequestModel, AgentProducts.class);
    }

    /**
     * This method is used to convert agentProducts dto to agentProducts entity
     * 
     * @author pabasara@simbiotiktech.com
     * @version 1.0
     * @since 1.0
     * @return agentProducts entity
     */
    public AgentProductsResponseModel agentProductsToAgentProductsDto(AgentProducts agentProducts){
        return this.modelMapper.map(agentProducts, AgentProductsResponseModel.class);
    }

}
