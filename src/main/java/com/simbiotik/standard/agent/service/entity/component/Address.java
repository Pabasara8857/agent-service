package com.simbiotik.standard.agent.service.entity.component;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.simbiotik.standard.common.service.ApplicationConstants;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Address class is used to define attributes Address entity
 * 
 * @author pabasara@simbiotiktech.com
 * @version 1.0
 * @since 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class Address implements Serializable {

  /**
   * addressLine1
   */
    @NotNull
    @Size(max = 300, min = 2, message = ApplicationConstants.MAX_ALLOWED_CHARACTERS_BETWEEN+"2 to 300")
    private String addressLine1;

    /**
     * addressLine2
     */
    @Size(max = 300, min = 2, message = ApplicationConstants.MAX_ALLOWED_CHARACTERS_BETWEEN+"2 to 300")
    private String addressLine2;

    /**
     * city
     */
    @NotNull
    @Size(max = 100, min = 2, message = ApplicationConstants.MAX_ALLOWED_CHARACTERS_BETWEEN+"2 to 100")
    private String city;

    /**
     * state
     */
    @NotNull
    @Size(max = 100, min = 2, message = ApplicationConstants.MAX_ALLOWED_CHARACTERS_BETWEEN+"2 to 100")
    private String state;

    /**
     * zipCode
     */
    @Size(max = 10,  message = ApplicationConstants.MAX_ALLOWED_CHARACTERS+"10")
    private int zipCode;

    public Address(String addressLine1, String addressLine2, String city,
        String state, int zipCode) {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Address address = (Address) o;
        return zipCode == address.zipCode && Objects.equals(addressLine1, address.addressLine1) && Objects.equals(addressLine2, address.addressLine2) && Objects
            .equals(city, address.city) && Objects.equals(state, address.state);
    }

    @Override public int hashCode() {
        return Objects.hash(addressLine1, addressLine2, city, state, zipCode);
    }
}
